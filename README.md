# Performance Tracker Demo

visualises the data collected with the [performance tracker](https://gitlab.com/pwrk/performance-tracker) in a Quasar app.

The app reads the data from a Strapi backend.

The environment can be configured in .env:

| Name                | Default                            |
| ------------------- | ---------------------------------- |
| VITE_STRAPI_URL     | http://localhost:1337/             |
| VITE_ROUTER_BASE    | /                                  |
| VITE_TOKEN          | API token from you strapi instance |
| VITE_ELASTIC_SERVER | Elastic search Server              |
| VITE_ELASTIC_TOKEN  | Elastic search Token               |
              

## Status

WIP (work in progess)

## Requirements

- yarn
- nodes

## Contribute

```bash
git clone https://gitlab.com/pwrk/performance-tracker-demo
cd performance-tracker-demo
yarn && yarn dev
```
