import 'regenerator-runtime/runtime';
import ElasticsearchAPIConnector from '@elastic/search-ui-elasticsearch-connector';

const now = new Date();
const last24hours = new Date(now - 24 * 60 * 60 * 1000);
const lastWeek = new Date(last24hours - 7 * 24 * 60 * 60 * 1000);
const lastMonth = new Date(lastWeek - 21 * 24 * 60 * 60 * 1000);
const lastTwoMonths = new Date(lastMonth - 30 * 24 * 60 * 60 * 1000);
const lastThreeMonths = new Date(lastTwoMonths - 30 * 24 * 60 * 60 * 1000);

const connector = new ElasticsearchAPIConnector({
  index: 'pageview',
  host: import.meta.env.VITE_ELASTIC_SERVER,
  apiKey: import.meta.env.VITE_ELASTIC_APIKEY,
});

const config = {
  debug: true,
  apiConnector: connector,
  alwaysSearchOnInitialLoad: true,
  searchQuery: {
    result_fields: {
      createdAt: {
        raw: {},
      },
      title: {
        raw: {},
      },
      updatedAt: {
        raw: {},
      },
      uri: {
        raw: {},
      },
      referrer: {
        raw: {},
      },
      job: {
        type: 'keyword',
        raw: {},
      },
      channel: {
        raw: {},
      },
      duration: {
        raw: {},
      },
      visibilityChange: {
        raw: {},
      },
      pageReload: {
        raw: {},
      },
      firstLoadingTime: {
        raw: {},
      },
      scrollMax: {
        raw: {},
      },
      linkClicked: {
        raw: {},
      },
      emailClicked: {
        raw: {},
      },
      applyClicked: {
        raw: {},
      },
      viewHeight: {
        raw: {},
      },
      viewWidth: {
        raw: {},
      },
    },
    disjunctiveFacets: ['job.keyword', 'channel.keyword', 'createdAt'],
    facets: {
      'channel.keyword': {
        name: 'label.channel',
        type: 'value',
        size: 10,
      },
      'job.keyword': {
        name: 'label.jobs',
        type: 'value',
        size: 10,
      },
      'duration.keyword': {
        name: 'label.duraion',
        type: 'value',
        size: 10,
      },
      createdAt: {
        name: 'label.date_published',
        type: 'range',
        ranges: [
          {
            from: last24hours.toISOString(),
            to: now.toISOString(),
            name: 'label.new',
          },
          {
            from: lastWeek.toISOString(),
            to: last24hours.toISOString(),
            name: 'label.last_week',
          },
          {
            from: lastMonth.toISOString(),
            to: lastWeek.toISOString(),
            name: 'label.last_month',
          },
          {
            from: lastTwoMonths.toISOString(),
            to: lastMonth.toISOString(),
            name: 'label.two_month',
          },
          {
            from: lastThreeMonths.toISOString(),
            to: lastTwoMonths.toISOString(),
            name: 'label.three_month',
          },
        ],
      },
    },
  },
};

export default config;
