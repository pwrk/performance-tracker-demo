export default {
  email: 'E-Mail',
  email_or_username: 'Email oder Benutzername',
  lastname: 'Nachname',
  login: 'Anmelden',
  logout: 'Abmelden',
  password: 'Kennwort',
  password_repeat: 'Kennwort wiederholen',
  profile: 'Benutzerprofil',
  forgot_password: 'Kennwort vergessen',
  register: 'Registrieren',
  reset_password: 'Kennwort zurücksetzen',
  username: 'Benutzername',
};
