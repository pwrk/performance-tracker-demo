export function hostname(val: string) {
  const hostname = url(val);
  return hostname?.hostname;
}

export function url(val: string) {
  return new URL(val ?? 'https://example.com');
}
