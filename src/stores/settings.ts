import {defineStore} from 'pinia';

type Settings = {
  counter: number;
  darkmode: string | boolean;
};

export const useSettingsStore = defineStore('settings', {
  state: (): Settings => ({
    counter: 0,
    darkmode: 'auto',
  }),

  getters: {
    doubleCount(state) {
      return state.counter * 2;
    },
  },

  actions: {
    increment() {
      this.counter++;
    },
  },
  persist: true,
});
