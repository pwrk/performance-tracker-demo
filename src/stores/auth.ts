import {defineStore} from 'pinia';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    counter: 0,
    token: null,
    user: {
      id: null,
      firstname: '',
      lastname: '',
      username: '',
      email: '',
      preferedLanguage: '',
      darkmode: 'auto',
      createdAt: null,
      updatedAt: null,
      company: null,
    },
    isLoggedIn: false,
  }),

  getters: {
    doubleCount(state) {
      return state.counter * 2;
    },
  },

  actions: {
    increment() {
      this.counter++;
    },
  },
  persist: true,
});
