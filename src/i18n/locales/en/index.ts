import label from './label';
import login from 'components/login/en';

export default {
  localeName: () => 'English',
  french: () => 'French',
  german: () => 'German',
  english: () => 'English',
  label: label,
  login: login,
  title: {
    all_pageviews_by_visit: () => 'All page views of the visit',
    pageview_details: () => 'Page view Details',
  },
  h: {
    time_to_load: () =>
      'Time span between the start of the tracker code and the load event of the browser at the first call.',
    time_to_read: () =>
      'The reading time is the sum of all time intervals during which a job ad was visible.',
    disable_tracking: () =>
      'Disable-tracking is appended to the URL to the job ad. This causes the tracker to disable itself.',
  },
  links: {
    docs: {
      title: () => 'Docs',
      caption: () => 'Performance Tracker',
    },
    gitlab: {
      title: () => 'Gitlab',
      caption: () => 'gitlab.com/pwrk/performance-tracker',
    },
  },
};
