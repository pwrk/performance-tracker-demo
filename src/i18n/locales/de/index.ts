import label from './label';
import login from 'components/login/de';

export default {
  localeName: () => 'Deutsch',
  french: () => 'Französisch',
  german: () => 'Deutsch',
  english: () => 'Englisch',
  label: label,
  login: login,
  title: {
    all_pageviews_by_visit: () => 'Alle Seitenansichten des Besuchs',
    pageview_details: () => 'Details zur Seitenansicht',
  },
  h: {
    time_to_load: () =>
      'Zeitspanne zwischen dem Starten des Tracker Code und dem load Event des Browsers beim ersten Aufruf',
    time_to_read: () =>
      'Die Lesezeit ist die Summe aller Zeitintervalle zu denen eine Stellenanzeige sichtbar war.',
    disable_tracking: () =>
      'An die URL zur Stellenanzeige wird disable-tracking angehängt. Dadurch deaktiviert der Tracker sich selbst.',
  },
  links: {
    docs: {
      title: () => 'Dokumentation',
      caption: () => 'Performance Tracker',
    },
    gitlab: {
      title: () => 'Gitlab',
      caption: () => 'gitlab.com/pwrk/performance-tracker',
    },
  },
};
