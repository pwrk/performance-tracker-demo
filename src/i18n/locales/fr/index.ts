import label from './label';
import login from 'components/login/fr';

export default {
  localeName: () => 'French',
  english: () => 'Anglais',
  french: () => 'Français',
  german: () => 'Allemand',
  label: label,
  login: login,
  title: {
    all_pageviews_by_visit: () => 'Toutes les pages vues de la visite',
    pageview_details: () => 'Détails de la vue de la page',
  },
  h: {
    time_to_load: () =>
      "Intervalle de temps entre le lancement du code du tracker et l'événement de chargement du navigateur lors du premier appel",
    time_to_read: () =>
      "Le temps de lecture est la somme de tous les intervalles de temps pendant lesquels une annonce d'emploi a été visible.",
    disable_tracking: () =>
      "L'URL de l'offre d'emploi est complétée par disable-tracking. Le tracker se désactive ainsi lui-même.",
  },
  links: {
    docs: {
      title: () => 'Documentation',
      caption: () => 'Performance Tracker',
    },
    gitlab: {
      title: () => 'Gitlab',
      caption: () => 'gitlab.com/pwrk/performance-tracker',
    },
  },
};
